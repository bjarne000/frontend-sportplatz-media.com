import { createRouter, createWebHistory } from "vue-router"
import HomeView from '../views/HomeView.vue'
import MediaView from '../views/MediaView.vue'
import NewsView from '../views/NewsView.vue'
import SoftwareView from '../views/SoftwareView.vue'
import NotFound from '../components/NotFound.vue'
import MarketingView from '../views/MarketingView.vue'
import ContactView from '../views/ContactView.vue'

const routes = [
    {
        path: '/',
        name: 'HomeView',
        component: HomeView
    },
    {
        path: '/not-found',
        name: 'NotFound',
        component: NotFound
    },
    {
        path: '/media',
        name: 'MediaView',
        component: MediaView
    },
    {
        path: '/news',
        name: 'NewsView',
        component: NewsView
    },
    {
        path: '/software',
        name: 'SoftwareView',
        component: SoftwareView
    },
    {
        path: '/marketing/:sitename?',
        name: 'MarketingView',
        component: MarketingView
    },
    {
        path: '/contact',
        name: 'ContactView',
        component: ContactView
    }
];

const router = createRouter({
    history: createWebHistory(),
    base: process.env.BASEURL,
    routes: routes
});

export default router;